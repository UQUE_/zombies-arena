using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UImanager : MonoBehaviour
{

    public static UImanager istance;

    public Text scoreText;
    public Text LifeText;

    int score = 0;
    int life = 100;
    private void Awake()
    {
        istance = this;
    }

    void Start()
    {
        
        scoreText.text = score.ToString() + " POINTS";
        
    }

    // Update is called once per frame
    public void addPoints()
    {
        score += 1;
        scoreText.text = score.ToString() + " POINTS";
      
    }

    public void subLife()
    {
        life -= 2;
        LifeText.text = life.ToString();
    }
}
