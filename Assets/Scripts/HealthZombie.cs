using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthZombie : MonoBehaviour
{
    [SerializeField] private int health = 100;

    void Start()
    {
        
    }
    void Update()
    {
        
    }

    public void Damage(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("Cannot have negative damage");
        }

        this.health -= amount;

        if (health <= 0)
        {
            Destroy(gameObject);
            UImanager.istance.addPoints();
        }

    }
}
