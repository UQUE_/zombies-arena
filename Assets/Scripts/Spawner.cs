using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject Enemy;
    [SerializeField] private GameObject Enemy1;

    [SerializeField] private float Enemytimer = 1.5f;
    [SerializeField] private float Enemy1timer = 2.5f;
    void Start()
    {
        StartCoroutine(spawnEnemy(Enemytimer, Enemy));
        StartCoroutine(spawnEnemy(Enemy1timer, Enemy1));
        StartCoroutine(spawnEnemy1(Enemytimer, Enemy));
        StartCoroutine(spawnEnemy1(Enemy1timer, Enemy1));

    }

    private IEnumerator spawnEnemy(float interval, GameObject enemy)
    {
        yield return new WaitForSeconds(interval);
        GameObject newEnemy = Instantiate(enemy, new Vector3(Random.Range(-18f, -10f), Random.Range(-18f, 18f), 0), Quaternion.identity);
        StartCoroutine(spawnEnemy(interval, enemy));
    }

    private IEnumerator spawnEnemy1(float interval, GameObject enemy)
    {
        yield return new WaitForSeconds(interval);
        GameObject newEnemy = Instantiate(enemy, new Vector3(Random.Range(18f, 10f), Random.Range(-18f, 18f), 0), Quaternion.identity);
        StartCoroutine(spawnEnemy(interval, enemy));
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
