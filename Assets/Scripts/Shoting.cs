using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject Bullet;

    public float bulletSpeed = 20f;

    // Update is called once per frame
    void Update()
    {
      if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }
    void Shoot()
    {
      GameObject bullet =  Instantiate(Bullet, firePoint.position, firePoint.rotation * Quaternion.Euler(0f, 0f, 90f));
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * bulletSpeed, ForceMode2D.Impulse);
    }
}

