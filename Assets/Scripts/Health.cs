using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Health : MonoBehaviour
{
    [SerializeField] private int health = 100;

    private int Maxhealth = 100;


    // Update is called once per frame
    void Update()
    {

    }

    public void SetHealth(int maxhealth, int health)
    {
        this.Maxhealth = maxhealth;
        this.health = health;
    }

    public void Damage(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("Cannot have negative damage");
        }

        this.health -= amount;

        if (health <= 0)
        {
            Die();
        }

    }

    private void Die()
    {
        SceneManager.LoadScene("Mainscene");
    }
}
