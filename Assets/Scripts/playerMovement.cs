using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class playerMovement : MonoBehaviour
{

    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public Camera cam;

    Vector2 movement;
    Vector2 mousePos;

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        movement.x = (Input.GetAxisRaw("Horizontal"));
        movement.y = (Input.GetAxisRaw("Vertical"));
    }

     void FixedUpdate()
    { 
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        rb.rotation = angle;
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
}
