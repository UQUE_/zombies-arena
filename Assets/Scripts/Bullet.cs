using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage;
    public float TimeTolive;

    private void Start()
    {
        Destroy(this.gameObject, TimeTolive);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (collision.GetComponent<HealthZombie>() != null)
            {
                collision.GetComponent<HealthZombie>().Damage(damage);
                Destroy(gameObject);

            }
        }
    }
}